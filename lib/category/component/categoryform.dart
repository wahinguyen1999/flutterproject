import 'package:flutter/material.dart';
import 'package:flutter_app/Model/products.dart';
import 'package:flutter_app/detail/productpage.dart';

class CategoryForm extends StatelessWidget {
  List<Products> products = Products.init();

  int id;

  CategoryForm({this.id});

  List<Products> getPfromCate(int id){
    List<Products> tmp = [];
    for(int i=0; i<products.length; i++){
      if(products[i].categoryID == id){
        tmp.add(products[i]);
      }
    }
    return tmp;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: ListView.builder(
          padding: const EdgeInsets.all(15),
          itemCount: getPfromCate(id).length,
          itemBuilder: (context, index) {
            return ListTile(
            onTap: () {
              Navigator.pushNamed(context, ProductPage.routeName,
                  arguments: ProductDetailsArguments(product: getPfromCate(id)[index]));
            },
              leading: Image.asset(getPfromCate(id)[index].image),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(getPfromCate(id)[index].title,style: TextStyle(fontWeight: FontWeight.bold)),
                  Text(getPfromCate(id)[index].price.toString()),
                  Text(getPfromCate(id)[index].description, overflow: TextOverflow.ellipsis),
                ],
              ),
            );
          }),
    );
  }
}

