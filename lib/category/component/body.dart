import 'package:flutter/material.dart';
import 'package:flutter_app/homepage/components/homeheader.dart';

import 'categoryform.dart';

class Body extends StatelessWidget {
  int categoryID;

  Body({this.categoryID});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: HomeHeader(),
      ),
      body: CategoryForm(id : this.categoryID),
    );
  }
}
