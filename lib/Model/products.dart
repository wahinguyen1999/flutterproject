import 'package:flutter/material.dart';

class Products {
  int id;
  int categoryID;
  String title;
  String description;
  String image;
  double price;

  Products({this.id, this.categoryID, this.title, this.description, this.image, this.price});
  static List<Products> init() {
    List<Products> data = [
      Products(
          id: 3,
          categoryID: 1,
          title: "Freeze Tea",
          description:
              "Tea is an aromatic beverage commonly prepared by pouring hot or boiling water over cured or fresh leaves of the Camellia sinensis, an evergreen shrub native to East Asia.[3] After water, it is the most widely consumed drink in the world.[4] There are many different types of tea; some, like Darjeeling and Chinese greens, have a cooling, slightly bitter, and astringent flavour,[5] while others have vastly different profiles that include sweet, nutty, floral, or grassy notes. Tea has a stimulating effect in humans primarily by its caffeine content",
          image: "assets/foods/ic_black_coffee.png",
          price: 8.0),
      Products(
          id: 4,
          categoryID: 3,
          title: "Milk Tea",
          description:
              "Tea is an aromatic beverage commonly prepared by pouring hot or boiling water over cured or fresh leaves of the Camellia sinensis, an evergreen shrub native to East Asia.[3] After water, it is the most widely consumed drink in the world.[4] There are many different types of tea; some, like Darjeeling and Chinese greens, have a cooling, slightly bitter, and astringent flavour,[5] while others have vastly different profiles that include sweet, nutty, floral, or grassy notes. Tea has a stimulating effect in humans primarily by its caffeine content",
          image: "assets/foods/ic_black_coffee.png",
          price: 8.0),
      Products(
          id: 5,
          categoryID: 4,
          title: "Coffee",
          description:
              "Founded as a bakery brand in Singapore in 2000 and listed on the SGX in 2003. It has since expanded to more than 1,000 retail stores spread across 17 markets. Its brand portfolio comprises BreadTalk, Toast Box, Food Republic, Food Junction, Din Tai Fung, Bread Society, Thye Moh Chan, The Icing Room, Sō and Nayuki in Singapore. Din Tai Fung, Song Fa Bak Kut Teh, Wu Pao Chun and Nayuki are franchised brands",
          image: "assets/foods/ic_black_coffee.png",
          price: 8.0),
      Products(
          id: 6,
          categoryID: 1,
          title: "milk & Tea 1",
          description:
              "Coffee is a brewed drink prepared from roasted coffee beans, the seeds of berries from certain Coffea species. When coffee berries turn from green to bright red in color – indicating ripeness – they are picked, processed, and dried.[2] Dried coffee seeds (referred to as \"beans\") are roasted to varying degrees, depending on the desired flavor. Roasted beans are ground and then brewed with near-boiling water to produce the beverage known as coffee.",
          image: "assets/foods/ic_black_coffee.png",
          price: 8.0),
      Products(
          id: 1,
          categoryID: 1,
          title: "Bread 1",
          description:
              "Founded as a bakery brand in Singapore in 2000 and listed on the SGX in 2003. It has since expanded to more than 1,000 retail stores spread across 17 markets. Its brand portfolio comprises BreadTalk, Toast Box, Food Republic, Food Junction, Din Tai Fung, Bread Society, Thye Moh Chan, The Icing Room, Sō and Nayuki in Singapore. Din Tai Fung, Song Fa Bak Kut Teh, Wu Pao Chun and Nayuki are franchised brands",
          image: "assets/foods/ic_black_coffee.png",
          price: 8.0),
      Products(
          id: 2,
          categoryID: 3,
          title: "Coffee",
          description:
              "Coffee is a brewed drink prepared from roasted coffee beans, the seeds of berries from certain Coffea species. When coffee berries turn from green to bright red in color – indicating ripeness – they are picked, processed, and dried.[2] Dried coffee seeds (referred to as \"beans\") are roasted to varying degrees, depending on the desired flavor. Roasted beans are ground and then brewed with near-boiling water to produce the beverage known as coffee.",
          image: "assets/foods/ic_black_coffee.png",
          price: 8.0),
      Products(
          id: 3,
          categoryID: 3,
          title: "Tea",
          description:
              "Coffee is a brewed drink prepared from roasted coffee beans, the seeds of berries from certain Coffea species. When coffee berries turn from green to bright red in color – indicating ripeness – they are picked, processed, and dried.[2] Dried coffee seeds (referred to as \"beans\") are roasted to varying degrees, depending on the desired flavor. Roasted beans are ground and then brewed with near-boiling water to produce the beverage known as coffee.",
          image: "assets/foods/ic_black_coffee.png",
          price: 8.0),
      Products(
          id: 4,
          categoryID: 2,
          title: "Bread",
          description:
              "Coffee is a brewed drink prepared from roasted coffee beans, the seeds of berries from certain Coffea species. When coffee berries turn from green to bright red in color – indicating ripeness – they are picked, processed, and dried.[2] Dried coffee seeds (referred to as \"beans\") are roasted to varying degrees, depending on the desired flavor. Roasted beans are ground and then brewed with near-boiling water to produce the beverage known as coffee.",
          image: "assets/foods/ic_black_coffee.png",
          price: 8.0),
      Products(
          id: 5,
          categoryID: 2,
          title: "Coffee",
          description:
              "Coffee is a brewed drink prepared from roasted coffee beans, the seeds of berries from certain Coffea species. When coffee berries turn from green to bright red in color – indicating ripeness – they are picked, processed, and dried.[2] Dried coffee seeds (referred to as \"beans\") are roasted to varying degrees, depending on the desired flavor. Roasted beans are ground and then brewed with near-boiling water to produce the beverage known as coffee.",
          image: "assets/foods/ic_black_coffee.png",
          price: 8.0),
      Products(
          id: 6,
          categoryID: 2,
          title: "milk & Tea 2",
          description:
              "Coffee is a brewed drink prepared from roasted coffee beans, the seeds of berries from certain Coffea species. When coffee berries turn from green to bright red in color – indicating ripeness – they are picked, processed, and dried.[2] Dried coffee seeds (referred to as \"beans\") are roasted to varying degrees, depending on the desired flavor. Roasted beans are ground and then brewed with near-boiling water to produce the beverage known as coffee.",
          image: "assets/foods/ic_black_coffee.png",
          price: 8.0),
    ];

    return data;
  }
}
